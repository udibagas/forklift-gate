"use strict";
const { Socket } = require("net");
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes, areaState, AreaState) => {
  class Gate extends Model {
    // socket client
    #client;

    static FORKLIFT = "forklift";

    static PERSON = "person";

    static associate(models) {
      Gate.hasMany(models.AccessLog);
    }

    get client() {
      return this.#client;
    }

    reconnect() {
      this.client.removeAllListeners();
      this.client.destroy();

      setTimeout(() => {
        this.connect();
      }, 1000);
    }

    connect() {
      const client = new Socket();
      const { port, host, name, type } = this;

      client.connect(port, host, () => {
        console.log(`Connected to ${name}`);
        if (type == Gate.PERSON) {
          this.client.write(Buffer.from(`\xA6OUT3ON\xA9`));
          this.client.write(Buffer.from(`\xA6OUT4OFF\xA9`));
        }

        if (type == Gate.FORKLIFT) {
          this.client.write(Buffer.from(`\xA6OUT2ON\xA9`));
          this.client.write(Buffer.from(`\xA6OUT3OFF\xA9`));
        }
      });

      client.on("data", (data) => {
        // remove header and footer
        data = data.toString().slice(1, -1);
        console.log(new Date(), ` - ${name} - ${data}`);
        const prefix = data[0];

        if (!["W", "X"].includes(prefix)) {
          // return console.log("Invalid input ", data);
          return;
        }

        const cardNumber = parseInt(data.slice(1), 16).toString();

        if (type == Gate.PERSON) {
          this.gatePersonSequence(cardNumber, prefix);
        }

        if (type == Gate.FORKLIFT) {
          this.gateForkliftSequence();
        }
      });

      client.on("error", (error) => {
        console.error(`Connection error. Reconnecting to ${name}...`);
        console.error(error);
        this.reconnect();
      });

      client.on("close", () => {
        console.error(`Connection closed. Reconnecting to ${name}...`);
        this.reconnect();
      });

      this.#client = client;
    }

    gatePersonSequence(cardNumber, prefix) {
      // TODO: check kartu valid atau ga
      let newLog;

      sequelize.models.AccessLog.findOne({
        where: { cardNumber },
        order: [["id", "desc"]],
      })
        .then((log) => {
          let access = "IN";

          // cek log terakhir
          if (log) {
            access = log.access == "IN" ? "OUT" : "IN";
          }

          if (
            access == "IN" &&
            areaState.status == AreaState.OCCUPIED_BY_FORKLIFT &&
            areaState.isOccupiedByForklift
          ) {
            throw "Sedang ada forklift!";
          }

          return sequelize.models.AccessLog.create({
            GateId: this.id,
            cardNumber,
            access,
          });
        })
        .then((log) => {
          newLog = log;
          const { cardNumber, access } = log;
          console.log(`Area clear - ${cardNumber} ${access}`);
          // Buka gate sesuai dia tap
          const command = prefix == "W" ? "TRIG1" : "TRIG2";
          this.client.write(Buffer.from(`\xA6${command}\xA9`));

          if (access == "IN") {
            areaState.status = AreaState.OCCUPIED_BY_PERSON;
            areaState.totalPersonInArea += 1;
            this.client.write(Buffer.from(`\xA6OUT3OFF\xA9`));
            this.client.write(Buffer.from(`\xA6OUT4ON\xA9`));
          }

          if (access == "OUT") {
            areaState.totalPersonInArea -=
              areaState.totalPersonInArea > 0 ? 1 : 0;
          }

          console.log(`${areaState.totalPersonInArea} orang di dalam area`);

          if (areaState.totalPersonInArea == 0) {
            areaState.status = AreaState.CLEAR;
            this.client.write(Buffer.from(`\xA6OUT3ON\xA9`));
            this.client.write(Buffer.from(`\xA6OUT4OFF\xA9`));
          }
        })
        .catch((err) => {
          console.error(err);
        });
    }

    gateForkliftSequence() {
      // TODO: check tag valid atau ga
      if (areaState.status == AreaState.OCCUPIED_BY_PERSON) {
        return console.error(
          `Sedang ada ${areaState.totalPersonInArea} orang di dalam area! Forklift dilarang masuk!`
        );
      }

      console.log("Forklift masuk!");
      areaState.status = AreaState.OCCUPIED_BY_FORKLIFT;
      areaState.lastOccupiedByForklift = new Date();
      // todo: ini harusnya ga terus terusan
      this.client.write(Buffer.from(`\xA6TRIG1\xA9`));
      this.client.write(Buffer.from(`\xA6OUT2OFF\xA9`));
      this.client.write(Buffer.from(`\xA6OUT3ON\xA9`));

      // kalau forklift sudah keluar set seperti semula indikatornya
      setTimeout(() => {
        if (!areaState.isOccupiedByForklift) {
          console.log("Forklift keluar!");
          this.client.write(Buffer.from(`\xA6OUT2ON\xA9`));
          this.client.write(Buffer.from(`\xA6OUT3OFF\xA9`));
        }
      }, 5001);
    }
  }

  Gate.init(
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Name is required",
          },
          notEmpty: {
            msg: "Name is required",
          },
        },
      },
      host: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Host is required",
          },
          notEmpty: {
            msg: "Host is required",
          },
          isIPv4: {
            msg: "Host must be valid IPv4",
          },
        },
      },
      port: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Port is required",
          },
          notEmpty: {
            msg: "Port is required",
          },
          isNumeric: {
            msg: "Port must be number",
          },
        },
      },
      type: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Type is required",
          },
          notEmpty: {
            msg: "Type is required",
          },
          isIn: {
            msg: "Invalid type",
            args: [["person", "forklift"]],
          },
        },
      },
    },
    {
      sequelize,
      modelName: "Gate",
    }
  );

  return Gate;
};
