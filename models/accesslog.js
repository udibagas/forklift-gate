"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class AccessLog extends Model {
    static associate(models) {
      AccessLog.belongsTo(models.Gate);
    }
  }
  AccessLog.init(
    {
      GateId: DataTypes.INTEGER,
      access: DataTypes.STRING,
      cardNumber: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "AccessLog",
    }
  );
  return AccessLog;
};
