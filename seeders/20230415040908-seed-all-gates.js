"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    await queryInterface.bulkInsert("Gates", [
      {
        name: "Person",
        host: "192.168.1.201",
        port: 5000,
        type: "person",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Forklift",
        host: "192.168.1.200",
        port: 5000,
        type: "forklift",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    await queryInterface.bulkDelete("Gates");
  },
};
