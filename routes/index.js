const router = require("express").Router();

const { index: home } = require("../controllers/app.controller");
const {
  login,
  logout,
  showLoginForm,
  changePassword,
  updatePassword,
} = require("../controllers/auth.controller");

router
  .get("/login", showLoginForm)
  .post("/login", login)
  .post("/logout", logout)
  .use(require("../middlewares/auth.middleware"))
  .get("/", home)
  .use("/cards", require("./cards"))
  .use("/gates", require("./gates"))
  .use("/users", require("./users"))
  .get("/change-password", changePassword)
  .post("/change-password", updatePassword);

module.exports = router;
