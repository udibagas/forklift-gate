const { index, show, update } = require("../controllers/gate.controller");
const router = require("express").Router();

router.get("/", index).get("/:id", show).post("/:id", update);

module.exports = router;
