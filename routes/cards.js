const {
  index,
  show,
  create,
  update,
  destroy,
} = require("../controllers/card.controller");
const router = require("express").Router();

router
  .get("/", index)
  .get("/:id", show)
  .post("/", create)
  .put("/:id", update)
  .delete("/:id", destroy);

module.exports = router;
