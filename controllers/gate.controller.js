"use strict";
const { Gate } = require("../models");
const { ValidationError } = require("sequelize");

class GateController {
  static async index(req, res) {
    const { user: username } = req.session;
    try {
      const gates = await Gate.findAll();
      res.render("app", {
        currentPath: req.path,
        pageTitle: "Gates",
        content: "gates",
        gates,
        username,
      });
    } catch (error) {
      res.status(500).send(err);
    }
  }

  static async show(req, res) {
    const { id } = req.params;
    const { error } = req.query;
    const { user: username } = req.session;

    try {
      const gate = await Gate.findByPk(id);
      if (!gate) throw new Error("Data not found");

      res.render("app", {
        currentPath: req.path,
        pageTitle: "Edit Gate",
        content: "edit-gate",
        gate,
        error,
        username,
      });
    } catch (error) {
      console.error(err);
      if (err instanceof Error) err = err.message;
      res.status(500).send(err);
    }
  }

  static async update(req, res) {
    const { id } = req.params;
    const { name, host, port, type } = req.body;

    try {
      const gate = await Gate.findByPk(id);
      if (!gate) throw new Error("Data not found");
      await gate.update({ name, host, port, type });
      res.redirect("/gates");
    } catch (error) {
      if (error instanceof ValidationError) {
        error = error.errors.map((e) => e.message);
      }

      res.redirect(`/gates/${id}?error=${error}`);
    }
  }
}

module.exports = GateController;
