"use strict";
const { User, Gate } = require("../models");

class AppController {
  static async index(req, res) {
    const user = await User.findOne();
    const gates = await Gate.findAll();
    const { user: username } = req.session;

    res.render("app", {
      currentPath: req.path,
      pageTitle: "Settings",
      content: "home",
      username,
      user,
      gates,
    });
  }
}

module.exports = AppController;
