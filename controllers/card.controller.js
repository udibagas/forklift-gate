"use strict";
const { Card } = require("../models");
const { ValidationError, Op } = require("sequelize");

class CardController {
  static async index(req, res) {
    const { user: username } = req.session;
    const { search } = req.query;
    const options = { order: [["name", "asc"]] };

    if (search) {
      options.where = {
        [Op.or]: {
          number: { [Op.iLike]: `%${search}%` },
          name: { [Op.iLike]: `%${search}%` },
        },
      };
    }

    try {
      const cards = await Card.findAll(options);
      res.render("app", {
        currentPath: req.path,
        pageTitle: "Cards",
        content: "cards",
        cards,
        username,
      });
    } catch (error) {
      res.status(500).send(err);
    }
  }

  static show(req, res) {
    Card.findByPk(req.params.id)
      .then((card) => {
        if (!card) throw new Error("Data not found");
        res.send(card);
      })
      .catch((err) => {
        console.error(err);
        if (err instanceof Error) err = err.message;
        res.status(500).send(err);
      });
  }

  static create(req, res) {
    const { name, host, port, type } = req.body;
    Card.create({ name, host, port, type })
      .then((card) => res.send(card))
      .catch((err) => {
        console.log(err);

        if (err instanceof ValidationError) {
          return res.status(400).send(err.errors.map((e) => e.message));
        }

        res.status(500).send(err);
      });
  }

  static update(req, res) {
    const { id } = req.params;
    const { name, host, port, type } = req.body;
    Card.findByPk(id)
      .then((card) => {
        if (!card) throw new Error("Data not found");
        return card.update({ name, host, port, type });
      })
      .then((card) => res.send(card))
      .catch((err) => {
        console.err(err);

        if (err instanceof ValidationError) {
          return res.status(400).send(err.errors.map((e) => e.message));
        }

        res.status(500).send(err);
      });
  }

  static destroy(req, res) {
    const { id } = req.params;
    Card.findByPk(id)
      .then((card) => {
        if (!card) throw new Error("Data not found");
        return card.destroy();
      })
      .then(() => res.send("Card has been deleted"))
      .catch((err) => {
        console.err(err);
        res.status(500).send(err);
      });
  }
}

module.exports = CardController;
