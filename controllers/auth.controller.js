"use strict";
const { ValidationError } = require("sequelize");
const { User } = require("../models");
var { compareSync } = require("bcryptjs");

class AuthController {
  static showLoginForm(req, res) {
    const { error } = req.query;
    res.render("login", { error });
  }

  static login(req, res) {
    const { username, password } = req.body;
    User.findOne({ where: { username } })
      .then((user) => {
        if (!user) throw new Error("Invalid user or password");

        if (!compareSync(password, user.password)) {
          throw new Error("Invalid user or password");
        }

        req.session.user = user.username;
        res.redirect("/");
      })
      .catch((err) => {
        console.log(err);

        if (err instanceof Error) {
          return res.redirect(`/login?error=${err.message}`);
        }

        res.status(500).send(err);
      });
  }

  static logout(req, res) {
    req.session = null;
    res.redirect("/login");
  }

  static changePassword(req, res) {
    const { user: username } = req.session;
    const { error, message } = req.query;
    const currentPath = req.path;

    res.render("app", {
      content: "change-password",
      pageTitle: "Change Password",
      username,
      currentPath,
      error,
      message,
    });
  }

  static async updatePassword(req, res) {
    const { oldPassword, password, confirmPassword } = req.body;

    try {
      const user = await User.findOne();

      // if (!compareSync(oldPassword, user.password)) {
      //   throw "Wrong old password";
      // }

      if (password !== confirmPassword) {
        throw "Password not match";
      }

      await user.update({ password });
      res.redirect(`/change-password?message=Password has been updated`);
    } catch (error) {
      if (error instanceof ValidationError) {
        error = error.errors.map((e) => e.message);
      }

      res.redirect(`/change-password?error=${error}`);
    }
  }
}

module.exports = AuthController;
