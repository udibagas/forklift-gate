"use strict";
const { User } = require("../models");
const { ValidationError } = require("sequelize");

class UserController {
  static index(req, res) {
    User.findAll()
      .then((users) => {
        res.render("app", {
          currentPath: req.path,
          pageTitle: "Users",
          content: "users",
          users,
        });
      })
      .catch((err) => res.status(500).send(err));
  }

  static show(req, res) {
    User.findByPk(req.params.id)
      .then((user) => {
        if (!user) throw new Error("Data not found");
        res.send(user);
      })
      .catch((err) => {
        console.error(err);
        if (err instanceof Error) err = err.message;
        res.status(500).send(err);
      });
  }

  static create(req, res) {
    const { username, password } = req.body;
    User.create({ username, password })
      .then((user) => res.send(user))
      .catch((err) => {
        console.log(err);

        if (err instanceof ValidationError) {
          return res.status(400).send(err.errors.map((e) => e.message));
        }

        res.status(500).send(err);
      });
  }

  static update(req, res) {
    const { id } = req.params;
    const { username, password } = req.body;
    User.findByPk(id)
      .then((user) => {
        if (!user) throw new Error("Data not found");
        return user.update({ username, password });
      })
      .then((user) => res.send(user))
      .catch((err) => {
        console.err(err);

        if (err instanceof ValidationError) {
          return res.status(400).send(err.errors.map((e) => e.message));
        }

        res.status(500).send(err);
      });
  }

  static destroy(req, res) {
    const { id } = req.params;
    User.findByPk(id)
      .then((user) => {
        if (!user) throw new Error("Data not found");
        return user.destroy();
      })
      .then(() => res.send("User has been deleted"))
      .catch((err) => {
        console.err(err);
        res.status(500).send(err);
      });
  }
}

module.exports = UserController;
