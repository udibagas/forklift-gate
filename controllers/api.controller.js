const { ValidationError } = require("sequelize");

class ApiController {
  constructor(model, dto) {
    this.model = model;
    this.dto = dto;
  }

  index(req, res) {
    this.model
      .findAll()
      .then((resources) => {
        res.json(resources);
      })
      .catch((err) => res.status(500).send(err));
  }

  show(req, res) {
    const { id } = req.params;
    this.model
      .findByPk(id)
      .then((resource) => {
        if (!resource) throw new Error("Data not found");
        res.json(resource);
      })
      .catch((err) => {
        if (err instanceof Error) {
          return res.status(404).send(err.message);
        }

        res.status(500).send(err);
      });
  }

  create(req, res) {
    const data = {};
    for (const key in this.dto) {
      data[key] = req.body[key];
    }

    this.model
      .create(data)
      .then((resource) => {
        res.json(resource);
      })
      .catch((err) => {
        if (err instanceof ValidationError) {
          return res
            .status(400)
            .json(err.errors.map(({ field, message }) => ({ field, message })));
        }

        res.status(500).send(err);
      });
  }

  update(req, res) {
    const { id } = req.params;
    const data = {};
    for (const key in this.dto) {
      data[key] = req.body[key];
    }

    this.model
      .findByPk(id)
      .then((resource) => {
        if (!resource) throw new Error("Data not found");
        return resource.update(data);
      })
      .then((resource) => res.json(resource))
      .catch((err) => {
        if (err instanceof ValidationError) {
          return res
            .status(400)
            .json(err.errors.map(({ field, message }) => ({ field, message })));
        }

        if (err instanceof Error) {
          return res.status(404).send(err.message);
        }

        res.status(500).send(err);
      });
  }

  destroy(req, res) {
    const { id } = req.params;
    this.model
      .findByPk(id)
      .then((resource) => {
        if (!resource) throw new Error("Data not found");
        return resource.destroy();
      })
      .then((resource) => {
        res.send(resource);
      })
      .catch((err) => {
        if (err instanceof Error) {
          return res.status(404).send(err.message);
        }

        res.status(500).send(err);
      });
  }
}

module.exports = ApiController;
