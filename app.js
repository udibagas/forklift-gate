require("dotenv").config();
const express = require("express");
var session = require("express-session");
const app = express();
const port = 3000;
const { Gate } = require("./models");

app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static("dist"));

app.use(
  session({
    resave: false,
    saveUninitialized: false,
    secret: process.env.SESSION_SECRET || "rahasia",
  })
);

app.use(require("./routes"));

Gate.findAll()
  .then((gates) => {
    gates.forEach((gate) => gate.connect());
  })
  .catch((err) => console.error(err));

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
